#ifndef _BOOL_C
#define _BOOL_C

typedef int bool;
#define false 0
#define true !false

#endif /* _BOOL_C */
